﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NameSearch
{
    class Person
    {

        public Person(String firstName, String lastName, int telephone)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.telephone = telephone;
        }

        private String firstName;
        private String lastName;
        private int telephone;

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public int Telephone { get => telephone; set => telephone = value; }

        public void printInfo()
        {
            Console.WriteLine($"| Firstname: {firstName} | Lastname: {lastName} | Phone: {telephone} |");
        }

    }
}
