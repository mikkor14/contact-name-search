﻿using System;
using System.Collections.Generic;
namespace NameSearch
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> contacts = new List<Person>();

            Person Mark = new Person("Mark", "Ohaio", 12341294);
            Person Lisa = new Person("Lisa", "Meapart", 54235913);
            Person Johnny = new Person("Johnny", "Waiseu", 98654923);
            Person Danny = new Person("Danny", "Crowdson", 78547092);
            Person TotalyNotDanny = new Person("Dan", "Fakeson", 43123451);

            contacts.Add(Mark);
            contacts.Add(Lisa);
            contacts.Add(Johnny);
            contacts.Add(Danny);
            contacts.Add(TotalyNotDanny);

            FindName("Lisa",contacts);
            FindName("Dan",contacts);
            FindName("aio",contacts);

        }

        static void FindName(String name, List<Person> list)
        {

            foreach (Person person in list)
            {
                if (person.FirstName.ToLower().Contains(name.ToLower()) || person.LastName.ToLower().Contains(name.ToLower()))
                {
                    person.printInfo();
                }
            }

        }

    }
}
